using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Threading
{
    public partial class MainForm : Form
    {

        Thread t1 = null, t2 = null, t3 = null;
        volatile bool Runing = false;

        private void TP(TextBox tb)
        {
            int n = 0;
            Random r = new Random();
            while (Runing)
            {
                this.BeginInvoke((Action)(() =>
                {
                    //tb.AppendText(n.ToString() + "\n");
                    tb.Text = n.ToString();
                }));
                n++;
                Thread.Sleep(r.Next(100,500));
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(Runing == false)
            {

                t1 = new Thread(() => TP(tb1));
                t2 = new Thread(() => TP(tb2));
                t3 = new Thread(() => TP(tb3));
                t1.Start();
                t2.Start();
                t3.Start();
                button1.Text = "Stop";
                Runing = true;
            }
            else
            {
                while (t1.IsAlive || t2.IsAlive || t3.IsAlive)
                {
                    Runing = false;
                }
                t1 = null;
                t2 = null;
                t3 = null;
                button1.Text = "Start";
            }


        }

        public MainForm()
        {
            InitializeComponent();
        }
    }
}
